# Contentful Type Versions API

An experimental API for managing Contentful content type versions.

![](./versions_api_aws.svg)

## Instructions
1. Apply Terraform from `.aws/terraform`
1. The hook's URL domain is provided by the output `cloudfront_default_domain`.
1. The hook's API key is an AWS secret.  Its name is provided by the output `api_key_secret_name` and its value can be retrieved from the AWS console.
1. Create a new webhook in Contentful:
    - **URL**: `PUT https://{cloudfront_default_domain}/type`
    - **Events**: Content Type - Create/Save/delete
    - **Payload**: Use default
1. Add a secret header:
    - **Key**: `Authorization`
    - **Value**: `Basic <api_key>`
1. Save the webhook.

The UI is hosted at `https://{cloudfront_default_domain}`.

## API

|Method|Path|Authentication|Description|
|---|---|---|---|
|`PUT`|`/type` | Basic |Ingest webhook payload |
|`GET`|`/type`| none | List all types |
|`GET`|`/type/_search/{term}`| none | Search for a specific type |
|`GET`|`/type/{id}`| none | Get all versions for a specific type |
|`GET`|`/type/{id}/{version}`| none | Get a specific version |
