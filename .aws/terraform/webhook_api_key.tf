resource "random_password" "api_key" {
    length           = 16
    special          = false
}

// store api key in secrets manager
resource "aws_secretsmanager_secret" "api_key_store" {
    name = "${var.project_name}-webhook-api-key-${terraform.workspace}"
}

resource "aws_secretsmanager_secret_version" "api_key_value" {
    secret_id     = aws_secretsmanager_secret.api_key_store.id
    secret_string = random_password.api_key.result
}
