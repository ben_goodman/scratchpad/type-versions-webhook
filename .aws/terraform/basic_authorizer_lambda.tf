resource "aws_iam_policy" "secrets_manager_access_policy" {
    name = "secrets-manager-access-policy-${random_id.cd_function_suffix.hex}"

    policy = jsonencode({
        Version = "2012-10-17"
        Statement = [{
                Effect = "Allow"
                Action = [
                    "secretsmanager:GetSecretValue"
                ]
                Resource = [
                    aws_secretsmanager_secret.api_key_store.arn,
                    "${aws_secretsmanager_secret.api_key_store.arn}/*"
                ]
            }]
    })
}


resource "aws_iam_role" "basic_auth_lambda_role" {
  name = "basic-auth-exec-role-${random_id.cd_function_suffix.hex}"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
        Effect = "Allow"
        Principal = {
            Service = "lambda.amazonaws.com"
        }
        Action = "sts:AssumeRole"
      }]
  })
}

resource "aws_iam_role_policy_attachment" "basic_auth_lambda_role_attachment" {
    role       = aws_iam_role.basic_auth_lambda_role.name
    policy_arn = aws_iam_policy.secrets_manager_access_policy.arn
}


module "auth_handler_payload" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws//modules/code_builder"
    version = "3.0.0"

    output_filename = "index.js"
    source_root     = "${path.module}/basic_auth_handler"
    build_command   = "make all"

    environment_variables = {
        SECRET_NAME = aws_secretsmanager_secret.api_key_store.name
    }
}

module "basic_authorizer_lambda_function" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws"
    version = "3.0.0"

    org              = "${var.resource_namespace}-${terraform.workspace}"
    project_name     = var.project_name
    lambda_payload   = module.auth_handler_payload.archive_file
    function_name    = "basic-auth-handler-${random_id.cd_function_suffix.hex}"
    function_handler = "index.handler"
    publish          = true
    memory_size      = 512
    runtime          = "nodejs20.x"
    role             = aws_iam_role.basic_auth_lambda_role
}

resource "aws_lambda_permission" "allow_gateway_invoke" {
    statement_id  = "lambda-auth-gateway-exec-${random_id.cd_function_suffix.hex}"
    action        = "lambda:InvokeFunction"
    function_name = module.basic_authorizer_lambda_function.name
    principal     = "apigateway.amazonaws.com"
    source_arn    = "${aws_apigatewayv2_api.default.execution_arn}/*/*"
}
