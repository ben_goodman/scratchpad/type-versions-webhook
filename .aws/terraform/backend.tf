terraform {
    backend "s3" {
        bucket = "tf-states-us-east-1-bgoodman"
        key    = "contentful-type-versions/terraform.tfstate"
        region = "us-east-1"
    }
}