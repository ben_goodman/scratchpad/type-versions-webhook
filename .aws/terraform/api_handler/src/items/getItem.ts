import { DynamoDBDocumentClient, QueryCommand, type QueryCommandOutput } from '@aws-sdk/lib-dynamodb';
import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
import type { PageOptions } from '../types'

const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME

export const getItem = async (
    id: string,
    {limit, from}: PageOptions = {}
): Promise<QueryCommandOutput> => {
    return await dynamo.send(
        new QueryCommand({
            TableName: TABLE_NAME,
            KeyConditionExpression: 'id = :id',
            ExpressionAttributeValues: {
                ":id": id
            },
            ScanIndexForward: false,
            Limit: limit ? parseInt(limit) : undefined,
            ExclusiveStartKey: from ? { id , version : from } : undefined
        })
    )
}
