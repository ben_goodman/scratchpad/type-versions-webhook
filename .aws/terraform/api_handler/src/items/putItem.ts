import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocumentClient, PutCommand, type PutCommandOutput } from "@aws-sdk/lib-dynamodb";
import type { ContentTypeJSON } from "../types";

const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME


export const putItem = async (body?: string): Promise<PutCommandOutput> => {
    if (body === undefined) {
        throw new Error('Body undefined.')
    }

    try {
        // this is the payload as determined by Contentful
        const webhookPayload = JSON.parse(body) as ContentTypeJSON

        const id = `${webhookPayload.sys.id}_${webhookPayload.sys.space.sys.id}`
        const version = `v${webhookPayload.sys.version}`

        // add some convenient non-key attributes.
        const itemAttributes = {
            id,
            space: webhookPayload.sys.space.sys.id,
            type: webhookPayload,
            dateCreated: new Date().toISOString(),
            isDeleted: false,
            searchName: webhookPayload.name.toLowerCase()
        }

        // create/update the 'latest' version entry.
        await dynamo.send(
            new PutCommand({
                TableName: TABLE_NAME,
                Item: {
                    version: 'latest',
                    ...itemAttributes,
                },
            })
        ).catch((err) => {
            console.log(`put default item to dynamo failed.`)
            console.log(err)
            throw err
        })

        // create a new entry for 'this' version.
        return await dynamo.send(
            new PutCommand({
                TableName: TABLE_NAME,
                Item: {
                    version,
                    ...itemAttributes
                },
            })
        ).catch((err) => {
            console.log(`put item to dynamo failed.`)
            console.log(err)
            throw err
        })

    } catch (error) {
        console.error('Error creating item', error)
        throw error
    }
}
