import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocumentClient, QueryCommand, type QueryCommandOutput } from "@aws-sdk/lib-dynamodb";
import type { PageOptions } from "../types";

const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME

export const listTypes = async ({
    limit,
    from
}: PageOptions): Promise<QueryCommandOutput> => {
    return await dynamo.send(
        new QueryCommand({
            TableName: TABLE_NAME,
            IndexName: 'LatestVersionIndex',
            KeyConditionExpression: 'version = :version',
            ExpressionAttributeValues: {
                ':version': 'latest'
            },
            ProjectionExpression: 'id, #space, isDeleted, dateCreated, searchName',
            ExpressionAttributeNames: {
                '#space': 'space'
            },
            Limit: limit ? parseInt(limit) : undefined,
            ExclusiveStartKey: from ? { id: from , version : 'latest' } : undefined
        })
    )
}
