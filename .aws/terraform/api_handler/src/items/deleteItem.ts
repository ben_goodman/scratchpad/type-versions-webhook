import { DynamoDBClient } from "@aws-sdk/client-dynamodb"
import { DynamoDBDocumentClient, PutCommand, type DeleteCommandOutput } from "@aws-sdk/lib-dynamodb"
import { ContentTypeJSON } from "../types"
import { getVersion } from "./getVersion"

const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME

export const deleteItem = async (body?: string): Promise<DeleteCommandOutput> => {
    if (body === undefined) {
        throw new Error('Body undefined.')
    }

    const webhookPayload = JSON.parse(body) as ContentTypeJSON

    const id = webhookPayload.sys.id

    const versionResp = await getVersion(id, 'latest')

    const itemData = versionResp?.Items?.[0]

    const type = {
        ...webhookPayload,
        // not included in default delete payload
        name: itemData!.type.name,
        description: itemData!.type.description,
    }

    return dynamo.send(
        new PutCommand({
            TableName: TABLE_NAME,
            Item: {
                id,
                version: 'latest',
                type,
                dateCreated: new Date().toISOString(),
                isDeleted: true
            },
        })
    ).catch((err) => {
        console.log(`deleting item ${id} failed`)
        console.log(err)
        throw err
    })
}
