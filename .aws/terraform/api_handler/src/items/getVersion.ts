import { DynamoDBDocumentClient, QueryCommand } from '@aws-sdk/lib-dynamodb';
import { DynamoDBClient } from '@aws-sdk/client-dynamodb';

const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME

export const getVersion = async (id: string, version: string) => {
    return await dynamo.send(
        new QueryCommand({
            TableName: TABLE_NAME,
            KeyConditionExpression: 'id = :id and version = :version',
            ExpressionAttributeValues: {
                ':id': id,
                ':version': version
            },
            ScanIndexForward: false
        })
    )
}
