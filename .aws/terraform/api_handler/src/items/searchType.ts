import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocumentClient, QueryCommand, type QueryCommandOutput } from "@aws-sdk/lib-dynamodb";
import type { PageOptions } from "../types";

const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME

export const searchType = async (
    searchTerm: string,
    { limit, from }: PageOptions
): Promise<QueryCommandOutput> => {
    return await dynamo.send(
        new QueryCommand({
            TableName: TABLE_NAME,
            IndexName: 'LatestVersionIndex',
            KeyConditionExpression: 'version = :version',
            FilterExpression: 'contains(searchName, :searchTerm)',
            ExpressionAttributeValues: {
                ':version': 'latest',
                ':searchTerm': searchTerm
            },
            Limit: limit ? parseInt(limit) : undefined,
            ExclusiveStartKey: from ? { id: from , version : 'latest' } : undefined
        })
    )
}
