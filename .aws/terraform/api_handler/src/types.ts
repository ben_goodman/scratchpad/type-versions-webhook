import type { QueryCommandOutput, ScanCommandOutput } from '@aws-sdk/lib-dynamodb'

export interface ListTypesIndex {
    id: string
    dateCreated: string
    isDeleted: boolean
    searchName: string
}

export interface DatabaseItem extends ListTypesIndex {
    version: string
    type: ContentTypeJSON
    searchName: string
    space: string
}

export interface ListTypeResponse extends Omit<QueryCommandOutput, 'Items'> {
    Items: ListTypesIndex[]
}

export interface PageOptions {
    limit?: string
    from?: string
}

export interface QueryTypeResponse extends Omit<QueryCommandOutput, 'Items'> {
    Items?: DatabaseItem[],
}

export interface SearchTypeResponse extends Omit<ScanCommandOutput, 'Items'> {
    Items?: DatabaseItem[],
}

export type CTFSys = {
    type: string,
    linkType: string,
    id: string
}

export interface CTFField {
    id: string,
    name: string,
    type: string,
    localized: boolean,
    required: boolean,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    validations: any[],
    disabled: boolean,
    omitted: boolean,
}

export interface ContentTypeJSON {
    sys: {
        space: {
            sys: CTFSys
        },
        id: string,
        type: string,
        createdAt: string,
        updatedAt: string,
        environment: {
            sys: CTFSys
        },
        publishedVersion: string,
        publishedAt: string,
        firstPublishedAt: string,
        createdBy: {
            sys: CTFSys
        },
        updatedBy: {
            sys: CTFSys
        },
        publishedCounter: string,
        version: string,
        publishedBy: {
            sys: CTFSys
        },
        urn: string
    },
    displayField: string
    name: string,
    description: string,
    fields: CTFField[]
}