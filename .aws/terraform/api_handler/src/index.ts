import type { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda'
import { listTypes, getItem, putItem, deleteItem, getVersion, searchType } from './items'

const headers = {"Content-Type": "application/json"}

export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    console.log('event', event)
    const  { resource, httpMethod } = event

    if (httpMethod === 'GET' && resource === '/type/_health') {
        return {
            headers,
            statusCode: 200,
            body: JSON.stringify({ message: 'Ok' }),
        }
    }

    // GET /type/_search/{searchTerm}
    if (httpMethod === 'GET' && resource === '/type/_search/{searchTerm}') {
        try {
            const { searchTerm } = event.pathParameters as any
            const { from, limit } = event.queryStringParameters || {}
            const result = await searchType(searchTerm, { from, limit })
            return {
                headers,
                statusCode: 200,
                body: JSON.stringify(result)
            }
        } catch(error) {
            return {
                headers,
                statusCode: 500,
                body: JSON.stringify(error),
            }
        }
    }

    // GET /type/{id}/version
    if (httpMethod === 'GET' && resource === '/type/{id}/{version}') {
        const { id, version } = event.pathParameters as any
        const item = await getVersion(id, version)
        if (item) {
            return {
                headers,
                statusCode: 200,
                body: JSON.stringify(item),
            }
        } else {
            return {
                headers,
                statusCode: 404,
                body: JSON.stringify({ message : 'Version not found.'}),
            }
        }
    }

    // GET /type/{id}
    if (httpMethod === 'GET' && resource === '/type/{id}') {
        const { id } = event.pathParameters as any
        const { from, limit } = event.queryStringParameters || {}
        const item = await getItem(id, {from, limit})
        return {
            headers,
            statusCode: 200,
            body: JSON.stringify(item),
        }
    }

    // GET /type
    if (httpMethod === 'GET' && resource === '/type') {
        try {
            const { from, limit } = event.queryStringParameters || {}
            const items = await listTypes({ from, limit })
            return {
                headers,
                statusCode: 200,
                body: JSON.stringify(items),
            }
        } catch (error) {
            return {
                headers,
                statusCode: 500,
                body: JSON.stringify(error),
            }
        }
    }

    // PUT /type
    if (httpMethod === 'PUT' && resource === '/type') {
        const contentfulWebhookTopic = event.headers['X-Contentful-Topic']
        const body = event.body && event.isBase64Encoded ? Buffer.from(event.body!, 'base64').toString('utf-8') : (event.body || undefined)

        if (
            contentfulWebhookTopic === 'ContentManagement.ContentType.save'
            || contentfulWebhookTopic === 'ContentManagement.ContentType.create'
        ) {
            const resp = await putItem(body)
            return {
                headers,
                statusCode: 200,
                body: JSON.stringify(resp),
            }
        } else if (contentfulWebhookTopic === 'ContentManagement.ContentType.delete') {
            const resp = await deleteItem(body)
            return {
                headers,
                statusCode: 200,
                body: JSON.stringify(resp),
            }
        } else {
            return {
                headers,
                statusCode: 401,
                body: JSON.stringify({ message: 'Unsupported webhook topic' }),
            }
        }
    }

    // default response
    return {
        headers,
        statusCode: 404,
        body: JSON.stringify({ message: 'Resource not found' }),
    }
}