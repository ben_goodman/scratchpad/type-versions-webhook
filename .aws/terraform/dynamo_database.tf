resource "aws_dynamodb_table" "default" {
    name             = "${var.dynamo_table_name}-${random_id.cd_function_suffix.hex}-${terraform.workspace}"
    billing_mode     = "PAY_PER_REQUEST"
    hash_key         = "id"
    range_key        = "version"

    // sys.id
    attribute {
        name = "id"
        type = "S"
    }

    // {version}/latest
    attribute {
        name = "version"
        type = "S"
    }

    attribute {
        name = "space"
        type = "S"
    }

    global_secondary_index {
        name               = "LatestVersionIndex"
        hash_key           = "version"
        projection_type    = "ALL"
    }

    global_secondary_index {
        name               = "SpaceIndex"
        hash_key           = "space"
        projection_type    = "ALL"
    }
}
