resource "aws_iam_policy" "dynamo_db_access_policy" {
    name = "dynamo-db-access-policy-${random_id.cd_function_suffix.hex}"

    policy = jsonencode({
        Version = "2012-10-17"
        Statement = [{
                Effect = "Allow"
                Action = [
                    "dynamodb:Scan",
                    "dynamodb:GetRecords",
                    "dynamodb:GetItem",
                    "dynamodb:PutItem",
                    "dynamodb:UpdateItem",
                    "dynamodb:DeleteItem",
                    "dynamodb:GetShardIterator",
                    "dynamodb:DescribeStream",
                    "dynamodb:ListStreams",
                    "dynamodb:Query"
                ]
                Resource = [
                    aws_dynamodb_table.default.arn,
                    "${aws_dynamodb_table.default.arn}/index/*"
                ]
            }]
    })
}


resource "aws_iam_role" "api_lambda_role" {
  name = "ctf-versions-exec-role-${random_id.cd_function_suffix.hex}"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
        Effect = "Allow"
        Principal = {
            Service = "lambda.amazonaws.com"
        }
        Action = "sts:AssumeRole"
      }]
  })
}

resource "aws_iam_role_policy_attachment" "ddb_lambda_role_attachment" {
    role       = aws_iam_role.api_lambda_role.name
    policy_arn = aws_iam_policy.dynamo_db_access_policy.arn
}

module "api_handler_payload" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws//modules/code_builder"
    version = "3.0.0"

    output_filename = "index.js"
    source_root     = "${path.module}/api_handler"
    build_command   = "make all"

    environment_variables = {
        TABLE_NAME  = aws_dynamodb_table.default.name
    }
}

module "api_lambda_function" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws"
    version = "3.0.0"

    org              = "${var.resource_namespace}-${terraform.workspace}"
    project_name     = var.project_name
    lambda_payload   = module.api_handler_payload.archive_file
    function_name    = "api-handler-${random_id.cd_function_suffix.hex}"
    function_handler = "index.handler"
    publish          = true
    memory_size      = 512
    role             = aws_iam_role.api_lambda_role
    runtime          = "nodejs20.x"
}

resource "aws_lambda_permission" "allow_default_gateway" {
    statement_id  = "ctf-versions-gateway-exec-${random_id.cd_function_suffix.hex}"
    action        = "lambda:InvokeFunction"
    function_name = module.api_lambda_function.name
    principal     = "apigateway.amazonaws.com"
    source_arn    = "${aws_apigatewayv2_api.default.execution_arn}/*/*"
}