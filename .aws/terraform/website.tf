module "website" {
    source  = "gitlab.com/ben_goodman/s3-website/aws"
    version = "3.0.0"

    org                                = "${var.resource_namespace}-${terraform.workspace}"
    project_name                       = var.project_name
    use_cloudfront_default_certificate = true
    default_cache_policy_id            = "658327ea-f89d-4fab-a63d-7e88639e58f6" # caching optimized
    default_response_headers_policy_id = "eaab4381-ed33-4a86-88ca-d9558dc6cd63" # CORS-with-preflight-and-SecurityHeadersPolicy

    additional_custom_origins = {
        "api" = {
            domain_name = trimsuffix(trimprefix(aws_apigatewayv2_api.default.api_endpoint, "https://"), "/")
            # dont override the origin_path if using the `$default` gateway stage.
            origin_path = aws_apigatewayv2_stage.default.id == "$default" ? null : "/${aws_apigatewayv2_stage.default.id}"
        }
    }

    ordered_cache_behaviors = {
        "api" = {
            path_pattern = "/type*"
            viewer_protocol_policy = "https-only"
            cache_policy_id = "4135ea2d-6df8-44a3-9df3-4b5a84be39ad" # disable caching for API
            origin_request_policy_id = "b689b0a8-53d0-40ab-baf2-68738e2966ac" # Managed-AllViewerExceptHostHeader
        }
    }
}
