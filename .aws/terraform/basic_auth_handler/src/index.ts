import { type APIGatewayRequestAuthorizerEventV2 } from 'aws-lambda'
import { SecretsManagerClient, GetSecretValueCommand } from '@aws-sdk/client-secrets-manager'

interface SimpleResponse {
    isAuthorized: boolean,
    context?: {}
}

const SECRET_NAME = process.env.SECRET_NAME
const client = new SecretsManagerClient()

export const handler = async (event: APIGatewayRequestAuthorizerEventV2): Promise<SimpleResponse> => {
    const authorization = event?.headers?.authorization

    if (authorization) {
        try {
            const token = authorization.replace(/\s+/g, ' ').trim().split(' ')[1]

            const response = await client.send(
                new GetSecretValueCommand({
                SecretId: SECRET_NAME,
                VersionStage: 'AWSCURRENT',
            }))

            return {
                'isAuthorized': response.SecretString === token
            }
        } catch (error) {
            // deny - invalid token
            console.error(error)
            return {
                'isAuthorized': false
            }
        }
    } else {
        // deny - no token
        return {
            'isAuthorized': false
        }
    }
}
