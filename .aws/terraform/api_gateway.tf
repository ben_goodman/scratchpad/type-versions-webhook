resource "aws_apigatewayv2_api" "default" {
    name          = "${var.project_name}-api-gateway-${terraform.workspace}"
    protocol_type = "HTTP"
}

resource "aws_cloudwatch_log_group" "default" {
    name = "/aws/${var.project_name}_gateway_${terraform.workspace}/${aws_apigatewayv2_api.default.name}"
    retention_in_days = 30
}

resource "aws_apigatewayv2_stage" "default" {
    api_id = aws_apigatewayv2_api.default.id
    name        = "$default"
    auto_deploy = true

    access_log_settings {
        destination_arn = aws_cloudwatch_log_group.default.arn

        format = jsonencode({
            requestId               = "$context.requestId"
            sourceIp                = "$context.identity.sourceIp"
            requestTime             = "$context.requestTime"
            protocol                = "$context.protocol"
            httpMethod              = "$context.httpMethod"
            resourcePath            = "$context.resourcePath"
            routeKey                = "$context.routeKey"
            status                  = "$context.status"
            responseLength          = "$context.responseLength"
            integrationErrorMessage = "$context.integrationErrorMessage"
            authorizerErrorMessage  = "$context.authorizer.error"
        })
    }
}

resource "aws_apigatewayv2_integration" "default" {
    api_id = aws_apigatewayv2_api.default.id
    integration_uri    = module.api_lambda_function.invoke_arn
    integration_type   = "AWS_PROXY"
    integration_method = "POST"
}

// basic authorizer
resource "aws_apigatewayv2_authorizer" "basic_auth" {
    api_id                            = aws_apigatewayv2_api.default.id
    authorizer_type                   = "REQUEST"
    identity_sources                  = ["$request.header.Authorization"]
    name                              = "${var.project_name}-basic-authorizer"
    authorizer_uri                    = module.basic_authorizer_lambda_function.invoke_arn
    authorizer_payload_format_version = "2.0"
    enable_simple_responses           = true
    authorizer_result_ttl_in_seconds  = 0
}

resource "aws_apigatewayv2_route" "health_check" {
    api_id = aws_apigatewayv2_api.default.id
    target    = "integrations/${aws_apigatewayv2_integration.default.id}"
    route_key = "GET /type/_health"
}

resource "aws_apigatewayv2_route" "type_search" {
    api_id = aws_apigatewayv2_api.default.id
    target    = "integrations/${aws_apigatewayv2_integration.default.id}"
    route_key = "GET /type/_search/{searchTerm}"
}

resource "aws_apigatewayv2_route" "get_todo_version" {
    api_id = aws_apigatewayv2_api.default.id
    target    = "integrations/${aws_apigatewayv2_integration.default.id}"
    route_key = "GET /type/{id}/{version}"
}

resource "aws_apigatewayv2_route" "get_todo" {
    api_id = aws_apigatewayv2_api.default.id
    target    = "integrations/${aws_apigatewayv2_integration.default.id}"
    route_key = "GET /type/{id}"
}

resource "aws_apigatewayv2_route" "get_todo_collection" {
    api_id = aws_apigatewayv2_api.default.id
    target    = "integrations/${aws_apigatewayv2_integration.default.id}"
    route_key = "GET /type"
}

resource "aws_apigatewayv2_route" "put_todo" {
    api_id = aws_apigatewayv2_api.default.id
    target    = "integrations/${aws_apigatewayv2_integration.default.id}"
    route_key = "PUT /type"

    authorization_type = "CUSTOM"
    authorizer_id      = aws_apigatewayv2_authorizer.basic_auth.id
}
