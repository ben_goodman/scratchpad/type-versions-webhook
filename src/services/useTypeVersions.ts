import type { ListTypeResponse, PageOptions, QueryTypeResponse, SearchTypeResponse,  } from '.aws/terraform/api_handler/src/types'


export const useTypeVersions = () => {
    return {
        listTypes: async ({from, limit}: PageOptions = {}): Promise<ListTypeResponse> => {
            const url = new URL('./type', window.location.origin)
            from && url.searchParams.append('from', from)
            limit && url.searchParams.append('limit', limit.toString())
            const resp = await fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            return await resp.json()
        },
        getType: async (id: string, version?: string): Promise<QueryTypeResponse> => {
            const url = `/type/${id}${version ? `/${version}` : ''}`
            const resp = await fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            return await resp.json()
        },
        searchType: async (searchTerm: string, {from, limit}: PageOptions = {}): Promise<SearchTypeResponse> => {
            const url = new URL(`./type/_search/${searchTerm}`, window.location.origin)
            from && url.searchParams.append('from', from)
            limit && url.searchParams.append('limit', limit.toString())
            const resp = await fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            return await resp.json()
        }
    }
}
