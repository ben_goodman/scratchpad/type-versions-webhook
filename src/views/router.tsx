import React from 'react'
import { Outlet, Route, createBrowserRouter, createRoutesFromElements } from 'react-router-dom'
import { Home } from 'src/views/Home'
import { Error } from 'src/views/Error'
import { Versions } from 'src/views/Versions'
import { BreadcrumbsProvider, Header } from 'src/components/Header'


const Main = () => {
    return (
        <BreadcrumbsProvider>
            <Header />
            <Outlet />
        </BreadcrumbsProvider>
    )
}

export const router = createBrowserRouter(
    createRoutesFromElements(
        <Route path='/' element={ <Main />}>
            <Route
                index
                element={ <Home /> }
            />
            <Route path='versions'>
                <Route path=':id' element={ <Versions /> }>
                    <Route path=':version' element={ <Versions/> } />
                </Route>
            </Route>
            <Route
                path='*'
                element={ <Error /> }
            />
        </Route>
    )
)