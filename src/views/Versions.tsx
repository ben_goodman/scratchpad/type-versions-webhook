import React, { useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { Accordion } from '@contentful/f36-accordion'
import { Spinner } from '@contentful/f36-spinner'
import { Text } from '@contentful/f36-typography'
import { Flex } from '@contentful/f36-core'
import ScrollToTop from 'react-scroll-to-top'
import JsonView from 'react18-json-view'
import 'react18-json-view/src/style.css'

import { useTypeVersions } from 'src/services/useTypeVersions'
import { QueryTypeResponse, } from '.aws/terraform/api_handler/src/types'
import { BreadcrumbsContext } from 'src/components/Header'

export const Versions = () => {
    const crumbsContext = React.useContext(BreadcrumbsContext)
    const navigate = useNavigate()
    const { id, version } = useParams()
    const versions = useTypeVersions()
    const [versionData, setVersionData] = React.useState<QueryTypeResponse>()

    useEffect(() => {
        if (id) {
            versions.getType(id, version).then(setVersionData)

            const crumbs = [{
                content: id.split('_')[0],
                url: `/versions/${id}`,
                onClick: () => navigate('../')
            }]

            if (version) {
                crumbs.push({
                    content: version,
                    url: `/versions/${id}/${version}`,
                    onClick: () => {}
                })
            }

            crumbsContext.setState?.(crumbs)
        }
    }, [])

    if (!versionData) {
        return (
            <Flex>
                <Text marginRight="spacingXs">Loading</Text>
                <Spinner />
            </Flex>
        )
    }

    return (
        <>
            {
                (versionData?.Count && versionData?.Count > 1) ?

                    versionData?.Items?.map( version => {
                        return (
                            <Accordion key={`version-${version.version}`}>
                                <Accordion.Item title={version.version}>
                                    <JsonView src={version.type}/>
                                </Accordion.Item>
                            </Accordion>
                        )
                    })

                    : <JsonView src={versionData?.Items![0]} />
            }
            <ScrollToTop smooth />
        </>
    )
}