import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Spinner } from '@contentful/f36-spinner'
import { Text } from '@contentful/f36-typography'
import { Flex } from '@contentful/f36-core'
import { useTypeVersions } from 'src/services/useTypeVersions'
import { ListTypeResponse } from '.aws/terraform/api_handler/src/types'
import { TypeInfoCard } from 'src/components/TypeInfoCard'


export const Home = () => {
    const navigate = useNavigate()
    const versions = useTypeVersions()
    const [data, setData] = React.useState<ListTypeResponse>()

    React.useEffect(() => {
        versions.listTypes().then((resp) => {
            setData(resp)
        })
    }, [])

    const handleItemClick = (id: string) => {
        navigate(`/versions/${id}`)
    }

    if (!data) {
        return (
            <Flex>
                <Text marginRight="spacingXs">Loading</Text>
                <Spinner />
            </Flex>
        )
    }

    return (
        <>
            {data?.Items?.map((item, i) => {
                console.log(item)
                return (
                    <TypeInfoCard
                        key={`item-row-${i}`}
                        id={item.id}
                        onClick={handleItemClick}
                    />
                )
            })
            }
        </>
    )
}
