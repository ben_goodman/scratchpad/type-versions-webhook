import React from 'react'
import { Autocomplete } from '@contentful/f36-autocomplete'
import { useTypeVersions } from 'src/services/useTypeVersions'

export const SearchBar = () => {
    const [searchResult, setSearchResult] = React.useState<string[]>()
    const [idMap, setIdMap] = React.useState({})
    const versions = useTypeVersions()

    const handleInputChange = async (value: string) => {
        versions.searchType(value).then(resp => {
            if (resp?.Items) {
                const tuples = resp?.Items.map( item => [item.searchName, item.id])
                setIdMap(Object.fromEntries(tuples))

                const typeNames = resp?.Items.map( item => item.searchName)
                setSearchResult(typeNames)
            }
        })
    }

    const handleSelectItem = (value: string) => {
        console.log(idMap)
        const id = idMap[value]
        window.location.pathname = `/versions/${id}`
    }

    return (
        <>
            <Autocomplete
                items={searchResult || []}
                onInputValueChange={handleInputChange}
                onSelectItem={handleSelectItem}
            />
        </>
    )
}