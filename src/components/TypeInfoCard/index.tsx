import React from 'react'
import { Card } from '@contentful/f36-card'
import { Badge } from '@contentful/f36-badge'
import { Flex } from '@contentful/f36-core'
import { Heading, Text } from '@contentful/f36-typography'
import { useTypeVersions } from 'src/services/useTypeVersions'
import { DatabaseItem } from '.aws/terraform/api_handler/src/types'

const PublishedBadge = () => <Badge variant={'positive'}>Published</Badge>
const DeletedBadge = () => <Badge variant={'negative'}>Deleted</Badge>

interface ItemInfoCardProps {
    id: string,
    onClick?: (id: string) => void
}

export const TypeInfoCard = ({
    id,
    onClick = () => {}
}: ItemInfoCardProps) => {
    const versions = useTypeVersions()
    const [versionData, setVersionData] = React.useState<DatabaseItem>()

    React.useEffect(() => {
        versions.getType(id, 'latest').then(data => {
            if (data?.Items && data.Items[0]) {
                setVersionData(data.Items[0])
            } else {
                throw new Error(`latest version for ${id} not found.`)
            }
        })
    }, [])

    return (
        <Card
            isLoading={!versionData}
            onClick={() => onClick(id)}
            style={{
                width: 'inherit'
            }}
        >
            {versionData &&
                <Flex flexDirection='column'>
                    <Flex
                        flexDirection='row'
                        justifyContent='space-between'
                        alignItems='baseline'
                    >
                        <Heading>{versionData.type.name}</Heading>
                        {versionData.isDeleted ? <DeletedBadge/> : <PublishedBadge/>}
                    </Flex>
                    <Text>{versionData.type.description}</Text>
                    <Text>Space ID: <code>{versionData.space}</code></Text>
                </Flex>
            }
        </Card>
    )
}