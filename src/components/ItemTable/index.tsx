import React from 'react'
import { ListTypeResponse } from '.aws/terraform/api_handler/src/types'
import { TypeInfoCard } from '../TypeInfoCard'

export interface ItemTableProps {
    items: ListTypeResponse
}

export const ItemTable = ({
    items,
}: ItemTableProps) => {
    const handleItemClick = async () => {

    }

    return (
        <>
            {items?.Items?.map((item, i) => {
                console.log(item)
                return (
                    <TypeInfoCard
                        key={`item-row-${i}`}
                        id={item.id}
                        onClick={handleItemClick}
                    />
                )
            })
            }
        </>
    )
}
