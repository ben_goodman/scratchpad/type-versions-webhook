import React from 'react'
import { Header as F36Header } from '@contentful/f36-header'
import { SearchBar } from '../SearchBar'


export interface Breadcrumb {
    content: string
    url: string
    onClick?: () => void
}

export interface IBreadcrumbsContext {
    state: Breadcrumb[]
    setState?: React.Dispatch<React.SetStateAction<Breadcrumb[]>>
}

const DEFAULT_CRUMB_STATE = [
    {
        content: '',
        url: '',
    },
]

export const BreadcrumbsContext = React.createContext<IBreadcrumbsContext>({
    state: DEFAULT_CRUMB_STATE,
})


export interface BreadcrumbsProviderProps extends React.PropsWithChildren {}

export const BreadcrumbsProvider = ({ children }: BreadcrumbsProviderProps) => {
    const [state, setState] = React.useState(DEFAULT_CRUMB_STATE)

    return (
        <BreadcrumbsContext.Provider value={{state, setState}}>
            {children}
        </BreadcrumbsContext.Provider>
    )
}

export const Header = () => {
    const { state } = React.useContext(BreadcrumbsContext)

    return (
        <F36Header
            filters={<SearchBar />}
            breadcrumbs={[
                {
                    content: 'Types',
                    url: '/'
                },
                ...state
            ]}
        />
    )
}